from django.db import models

# Create your models here.


class Manufacturer(models.Model):
	"""
		To save Manufacturer details
	"""
	manufaturer_code = models.CharField(max_length=20, primary_key=True)
	manufaturer_name = models.CharField(max_length=50)
	manufaturer_details = models.TextField()

	def __str__(self):
		return self.manufaturer_code


class Model(models.Model):
	"""
		To save Models of Car
	"""
	manufaturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)
	model_code = models.CharField(max_length=20, primary_key=True)
	model_name = models.CharField(max_length=50)
	daily_hire_rate = models.FloatField()

	def __str__(self):
		return self.model_code


# Defines Gender Choices
GENDER_CHOICES = (
	('Male', 'Male'),
	('Female', 'Female')
)


class Customer(models.Model):
	"""
		To save Customer details
	"""
	customer_id = models.CharField(max_length=50, primary_key=True)
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	title = models.CharField(max_length=50)
	gender = models.CharField(max_length=20, choices=GENDER_CHOICES)
	email = models.CharField(max_length=50)
	phone_number = models.CharField(max_length=20)
	address_line_1 = models.CharField(max_length=50)
	address_line_2 = models.CharField(max_length=50)
	address_line_3 = models.CharField(max_length=50)
	city = models.CharField(max_length=50)
	state = models.CharField(max_length=50)
	other_customer_details = models.TextField()

	def __str__(self):
		return self.customer_id


class Car(models.Model):
	"""
		To save Car details
	"""
	car_model = models.ForeignKey(Model, on_delete=models.CASCADE)
	customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
	license_number = models.CharField(max_length=50, primary_key=True)
	current_milege = models.FloatField()
	engine_size = models.DecimalField(decimal_places=2, max_digits=10, default=0.0)
	other_car_details = models.TextField()

	def __str__(self):
		return self.license_number


class Mechanic(models.Model):
	"""
		To save Details of Mechanic Persons
	"""
	mechanic_id = models.CharField(max_length=50, primary_key=True)
	mechanic_name = models.CharField(max_length=50)
	other_mechanic_details = models.TextField()

	def __str__(self):
		return self.mechanic_id


class Booking(models.Model):
	"""
		To save Booking details
	"""
	car = models.ForeignKey(Car, on_delete=models.CASCADE)
	mechanic = models.ManyToManyField(Mechanic)
	customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
	booking_id = models.CharField(max_length=50, primary_key=True)
	date_of_service = models.DateField()
	whether_payment_recieved = models.BooleanField(default=False)
	other_booking_details = models.TextField()

	def __str__(self):
		return self.booking_id