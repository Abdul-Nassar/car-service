from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Manufacturer)
admin.site.register(Model)
admin.site.register(Customer)
admin.site.register(Car)
admin.site.register(Mechanic)
admin.site.register(Booking)
