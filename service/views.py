"""
	Describes Views for CRUD operations of Models defined in Service App
"""
from django.shortcuts import render
from rest_framework.generics import (
	ListCreateAPIView, RetrieveUpdateDestroyAPIView
)
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import render
from .models import *
from .serializers import *
from django.http.response import HttpResponseRedirect

class CsrfExemptSessionAuthentication(SessionAuthentication):
	"""
		view for excepting CSRF in requests
	"""
	def enforce_csrf(self, request):
		return  
# Create your views here.

class ManufacturerListSerializerView(ListCreateAPIView):
	"""
		view for list & create 'Manufacturer' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Manufacturer.objects.all()
	serializer_class = ManufacturerSerializer
	

class ManufacturerSerializerView(RetrieveUpdateDestroyAPIView):
	"""
		view for retrieve, update & delete  'Manufacturer' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Manufacturer.objects.all()
	serializer_class = ManufacturerSerializer


class CarModelListSerializerView(ListCreateAPIView):
	"""
		view for list & create 'Car Models' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Model.objects.all()
	serializer_class = CarModelSerializer


class CarModelSerializerView(RetrieveUpdateDestroyAPIView):
	"""
		view for retrieve, update & delete  'Car Models' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Model.objects.all()
	serializer_class = CarModelSerializer


class CustomerListSerializerView(ListCreateAPIView):
	"""
		view for list & create 'Customer' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Customer.objects.all()
	serializer_class = CustomerSerializer


class CustomerSerializerView(RetrieveUpdateDestroyAPIView):
	"""
		view for retrieve, update & delete  'Customer' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Customer.objects.all()
	serializer_class = CustomerSerializer


class CarListSerializerView(ListCreateAPIView):
	"""
		view for list & create 'Car' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Car.objects.all()
	serializer_class = CarSerializer


class CarSerializerView(RetrieveUpdateDestroyAPIView):
	"""
		view for retrieve, update & delete  'Car' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Car.objects.all()
	serializer_class = CarSerializer

class MechanicListSerializerView(ListCreateAPIView):
	"""
		view for list & create 'Mechanic' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Mechanic.objects.all()
	serializer_class = MechanicSerializer


class MechanicSerializerView(RetrieveUpdateDestroyAPIView):
	"""
		view for retrieve, update & delete  'Mechanic' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Mechanic.objects.all()
	serializer_class = MechanicSerializer


class BookingListSerializerView(ListCreateAPIView):
	"""
		view for list & create 'Booking' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Booking.objects.all()
	serializer_class = BookingSerializer


class BookingSerializerView(RetrieveUpdateDestroyAPIView):
	"""
		for retrieve, update & delete  'Booking' model
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	queryset = Booking.objects.all()
	serializer_class = BookingSerializer
