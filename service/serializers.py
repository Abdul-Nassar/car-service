"""
	Describes Serializers for all models in service App
"""
from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from .models import *

class ManufacturerSerializer(ModelSerializer):
	class Meta:
		model = Manufacturer
		fields = '__all__'


class CarModelSerializer(ModelSerializer):
	class Meta:
		model = Model
		fields = '__all__'


class CustomerSerializer(ModelSerializer):
	class Meta:
		model = Customer
		fields = '__all__'


class CarSerializer(ModelSerializer):
	class Meta:
		model = Car
		fields = '__all__'


class MechanicSerializer(ModelSerializer):
	class Meta:
		model = Mechanic
		fields = '__all__'


class BookingSerializer(ModelSerializer):
	class Meta:
		model = Booking
		fields = '__all__'