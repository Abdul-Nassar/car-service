"""
    Defines Uls for views in Service app
"""
from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^manufaturers/',
    	ManufacturerListSerializerView.as_view(), name='manufaturers'), #url for list & create 'Manufacturer' model
    url(r'^manufacturer/(?P<pk>[-\S]+)/$',
    	ManufacturerSerializerView.as_view(), name='manufacturer'), #url for retrieve, update & delete  'Manufacturer' model
    url(r'^car_models/',
    	CarModelListSerializerView.as_view(), name='car_models'), #url for list & create 'Car Models' model
    url(r'^car_model/(?P<pk>[-\S]+)/$',
    	CarModelSerializerView.as_view(), name='car_model'), #url for retrieve, update & delete  'Car Models' model
    url(r'^customers/',
    	CustomerListSerializerView.as_view(), name='customers'), #url for list & create 'Customer' model
    url(r'^customer/(?P<pk>[-\S]+)/$',
    	CustomerSerializerView.as_view(), name='customer'), #url for retrieve, update & delete  'Customer' model
    url(r'^cars/', CarListSerializerView.as_view(), name='cars'), #url for list & create 'Car' model
    url(r'^car/(?P<pk>[-\S]+)/$', CarSerializerView.as_view(), name='car'), #url for retrieve, update & delete  'Car' model
    url(r'^mechanics/',
    	MechanicListSerializerView.as_view(), name='mechanics'), #url for list & create 'Mechanic' model
    url(r'^mechanic/(?P<pk>[-\S]+)/$',
    	MechanicSerializerView.as_view(), name='mechanic'), #url for retrieve, update & delete  'Mechanic' model
    url(r'^bookings/',
    	BookingListSerializerView.as_view(), name='bookings'), #url for list & create 'Booking' model
    url(r'^booking/(?P<pk>[-\S]+)/$',
    	BookingSerializerView.as_view(), name='booking'), #url for retrieve, update & delete  'Booking' model
]