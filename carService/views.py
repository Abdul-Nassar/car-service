"""
	Car Service Views
"""
from django.shortcuts import render
from django.contrib.auth.models import User

# Redirects to this function from Google Authentication url
def home(request):
	# Checking for whether the user trying to logins is Admin or not
	if User.objects.get(username=request.user).is_superuser:
		return render(request, 'home.html')
	else:
		return render(request, 'error.html')
	