$(document).ready(function () {
	$(document).on('click', '#btn_manufacturer', function(){
		get_manufactures();
	});
    $(document).on('click', '#delete_manufacture', function(){
        var code = $(this).attr('data-code');
        $.ajax({
            url: "/service/manufacturer/" + code + '/',
            type: "DELETE",
            success: function(data){
                get_manufactures();
            }
        })
    });

    $(document).on('click', '#edit_manufacture', function(){
        console.log($(this).attr('data-code'));
        var code = $(this).attr('data-code');
        $.ajax({
            url: "/service/manufacturer/" + code + '/',
            type: "GET",
            success: function(data){
                console.log(data);
                $('#manufaturer_code').val(data.manufaturer_code);
                $('#manufaturer_name').val(data.manufaturer_name);
                $('#manufaturer_details').val(data.manufaturer_details);
                $('#save_manufaturer').attr("data-code", data.manufaturer_code);
                // $('#save_manufaturer').data('code', data.manufaturer_code); 
            }
        })
    });
	
    function get_manufactures(){
        $('#car_div').hide()
        $('#models_div').hide();
        $('#customer_div').hide();
        $('#mechanic_div').hide();
        $('#booking_div').hide();
        $('#manufaturers_div').show();
		$.ajax({
            url: "/service/manufaturers/",
            type: "GET",
            success: function(data){
            	$("#manufaturer_data").empty();
            	$.each(data, function(key, value) {
            		var tbl_data = '<tr>';
            		tbl_data += '<td>' + value.manufaturer_code + '</td>';
            		tbl_data += '<td>' + value.manufaturer_name + '</td>';
            		tbl_data += '<td>' + value.manufaturer_details + '</td>';
                    tbl_data += '<td><button data-code="'+ value.manufaturer_code +'" id="delete_manufacture">Delete</button><button data-code="'+ value.manufaturer_code +'" id="edit_manufacture">Edit</button></td>';
            		tbl_data += '</tr>';
            		$(tbl_data).appendTo("#manufaturer_data");
            	});
            }
        })
	}

    $(document).on('click', '#save_manufaturer', function(){
        console.log($(this).data('code'));
        var code = $(this).data('code');
        var saveData = {
            'manufaturer_code': $('#manufaturer_code').val(),
            'manufaturer_name': $('#manufaturer_name').val(),
            'manufaturer_details': $('#manufaturer_details').val()
        }
        if (code){
            $.ajax({
                url: "/service/manufacturer/" + code + '/',
                type: "PUT",
                dataType: "json",
                data:saveData, 
                success: function(data){
                    $("#manufaturer_data_div").find('input').val('');
                    $("#manufaturer_data_div").find('textarea').val('');
                    $("#manufaturer_data_div").find('select').val('');
                    get_manufactures();
                }
            });
        }else{
            $.ajax({
                url: "/service/manufaturers/",
                type: "POST",
                dataType: "json",
                data:saveData, 
                success: function(data){
                    $("#manufaturer_data_div").find('input').val('');
                    $("#manufaturer_data_div").find('textarea').val('');
                    $("#manufaturer_data_div").find('select').val('');
                    get_manufactures();
                }
            });
        }
    });
	
	$(document).on('click', '#btn_model', function(){
		get_car_models();
	});

    $(document).on('click', '#delete_model', function(){
        var code = $(this).attr('data-code');
        $.ajax({
            url: "/service/car_model/" + code + '/',
            type: "DELETE",
            success: function(data){
                get_car_models();
            }
        })
    })
	function get_car_models(){
        $('#car_div').hide()
        $('#manufaturers_div').hide();
        $('#customer_div').hide();
        $('#mechanic_div').hide();
        $('#booking_div').hide();
        $('#models_div').show();
		$.ajax({
            url: "/service/car_models/",
            type: "GET",
            success: function(data){
            	console.log(data)
            	$("#model_data").empty();
            	$.each(data, function(key, value) {
            		var tbl_data = '<tr>';
            		tbl_data += '<td>' + value.model_code + '</td>';
            		tbl_data += '<td>' + value.model_name + '</td>';
            		tbl_data += '<td>' + value.daily_hire_rate + '</td>';
            		tbl_data += '<td>' + value.manufaturer + '</td>';
                    tbl_data += '<td><button data-code="'+ value.model_code +'" id="delete_model">Delete</button></td>';
                    tbl_data += '</tr>';
            		$(tbl_data).appendTo("#model_data");
            	});
            }
        })
	}

    $(document).on('click', '#edit_customer', function(){
        console.log($(this).attr('data-customer_id'));
        var customer_id = $(this).attr('data-customer_id');
        $.ajax({
            url: "/service/customer/" + customer_id + '/',
            type: "GET",
            success: function(data){
                console.log(data);
                $('#customer_id').val(data.customer_id);
                $('#first_name').val(data.first_name);
                $('#last_name').val(data.last_name);
                $('#title').val(data.title);
                $('#gender').val(data.gender);
                $('#email').val(data.email);
                $('#phone_number').val(data.phone_number);
                $('#address_line_1').val(data.address_line_1);
                $('#address_line_2').val(data.address_line_2);
                $('#address_line_3').val(data.address_line_3);
                $('#city').val(data.city);
                $('#state').val(data.state);
                $('#other_customer_details').val(data.other_customer_details);
                $('#save_customer').attr("data-customer_id", data.customer_id);
            }
        })
    });

    $(document).on('click', '#save_customer', function(){
        console.log($(this).data('customer_id'));
        var customer_id = $(this).data('customer_id');
        var saveData = {
            'customer_id': $('#customer_id').val(),
            'first_name': $('#first_name').val(),
            'last_name': $('#last_name').val(),
            'title':$('#title').val(),
            'gender':$('#gender').val(),
            'email':$('#email').val(),
            'phone_number':$('#phone_number').val(),
            'address_line_1':$('#address_line_1').val(),
            'address_line_2':$('#address_line_2').val(),
            'address_line_3':$('#address_line_3').val(),
            'city':$('#city').val(),
            'state':$('#state').val(),
            'other_customer_details':$('#other_customer_details').val()
        }
        if (customer_id){
            $.ajax({
                url: "/service/customer/" + customer_id + '/',
                type: "PUT",
                dataType: "json",
                data:saveData, 
                success: function(data){
                    $("#customer_data_div").find('input').val('');
                    $("#customer_data_div").find('textarea').val('');
                    $("#customer_data_div").find('select').val('');
                    get_customers();
                }
            });
        }else{
            $.ajax({
                url: "/service/customers/",
                type: "POST",
                dataType: "json",
                data:saveData, 
                success: function(data){
                    $("#customer_data_div").find('input').val('');
                    $("#customer_data_div").find('textarea').val('');
                    $("#customer_data_div").find('select').val('');
                    get_customers();
                }
            });
        }
    });

    $(document).on('click', '#delete_customer', function(){
        var customer_id = $(this).attr('data-customer_id');
        $.ajax({
            url: "/service/customer/" + customer_id + '/',
            type: "DELETE",
            success: function(data){
                get_customers();
            }
        })
    })
	$(document).on('click', '#btn_customer', function(){
		get_customers();
	});
	function get_customers(){
        $('#car_div').hide()
        $('#models_div').hide();
        $('#manufaturers_div').hide();
        $('#mechanic_div').hide();
        $('#booking_div').hide();
        $('#customer_div').show();
		$.ajax({
            url: "/service/customers/",
            type: "GET",
            success: function(data){
            	console.log(data)
            	$("#customer_data").empty();
            	$.each(data, function(key, value) {
            		var tbl_data = '<tr>';
            		tbl_data += '<td>' + value.customer_id + '</td>';
            		tbl_data += '<td>' + value.first_name + '</td>';
            		tbl_data += '<td>' + value.last_name + '</td>';
            		tbl_data += '<td>' + value.title + '</td>';
            		tbl_data += '<td>' + value.gender + '</td>';
            		tbl_data += '<td>' + value.email + '</td>';
            		tbl_data += '<td>' + value.phone_number + '</td>';
            		tbl_data += '<td>' + value.address_line_1 + '</td>';
            		tbl_data += '<td>' + value.address_line_2 + '</td>';
            		tbl_data += '<td>' + value.address_line_3 + '</td>';
            		tbl_data += '<td>' + value.city + '</td>';
            		tbl_data += '<td>' + value.state + '</td>';
            		tbl_data += '<td>' + value.other_customer_details + '</td>';
                    tbl_data += '<td><button data-customer_id="'+ value.customer_id +'" id="delete_customer">Delete</button><button data-customer_id="'+ value.customer_id +'" id="edit_customer">Edit</button></td>';
                    tbl_data += '</tr>';
            		$(tbl_data).appendTo("#customer_data");
            	});
            }
        })
	}
    $(document).on('click', '#delete_car', function(){
        var license_number = $(this).attr('data-license_number');
        $.ajax({
            url: "/service/car/" + license_number + '/',
            type: "DELETE",
            success: function(data){
                get_cars();
            }
        })
    })
	$(document).on('click', '#btn_car', function(){
		get_cars();
	});
	function get_cars(){
        $('#models_div').hide();
        $('#manufaturers_div').hide();
        $('#customer_div').hide();
        $('#mechanic_div').hide();
        $('#booking_div').hide();
		$.ajax({
            url: "/service/cars/",
            type: "GET",
            success: function(data){
            	console.log(data)
            	$("#car_data").empty();
            	$.each(data, function(key, value) {
            		var tbl_data = '<tr>';
            		tbl_data += '<td>' + value.car_model + '</td>';
            		tbl_data += '<td>' + value.customer + '</td>';
            		tbl_data += '<td>' + value.license_number + '</td>';
            		tbl_data += '<td>' + value.current_milege + '</td>';
            		tbl_data += '<td>' + value.engine_size + '</td>';
            		tbl_data += '<td>' + value.other_car_details + '</td>';
                    tbl_data += '<td><button data-license_number="'+ value.license_number +'" id="delete_car">Delete</button></td>';
                    tbl_data += '</tr>';
            		$(tbl_data).appendTo("#car_data");
            		$('#car_div').show()
            	});
            }
        })
	}
    $(document).on('click', '#delete_mechanic', function(){
        var mechanic_id = $(this).attr('data-mechanic_id');
        $.ajax({
            url: "/service/mechanic/" + mechanic_id + '/',
            type: "DELETE",
            success: function(data){
                get_mechanics();
            }
        })
    })
	$(document).on('click', '#btn_mechanic', function(){
		get_mechanics();
	});
	function get_mechanics(){
        $('#booking_div').hide();
        $('#car_div').hide();
        $('#models_div').hide();
        $('#manufaturers_div').hide();
        $('#customer_div').hide();
        $('#mechanic_div').show();
		$.ajax({
            url: "/service/mechanics/",
            type: "GET",
            success: function(data){
            	console.log(data)
            	$("#mechanic_data").empty();
            	$.each(data, function(key, value) {
            		var tbl_data = '<tr>';
            		tbl_data += '<td>' + value.mechanic_id + '</td>';
            		tbl_data += '<td>' + value.mechanic_name + '</td>';
            		tbl_data += '<td>' + value.other_mechanic_details + '</td>';
                    tbl_data += '<td><button data-mechanic_id="'+ value.mechanic_id +'" id="delete_mechanic">Delete</button></td>';
                    tbl_data += '</tr>';
            		$(tbl_data).appendTo("#mechanic_data");
            	});
            }
        })
	}
    $(document).on('click', '#delete_booking', function(){
        var booking_id = $(this).attr('data-booking_id');
        $.ajax({
            url: "/service/booking/" + booking_id + '/',
            type: "DELETE",
            success: function(data){
                get_bookings();
            }
        })
    })
	$(document).on('click', '#btn_booking', function(){
		get_bookings();
	});
	function get_bookings(){
        $('#car_div').hide()
        $('#models_div').hide();
        $('#manufaturers_div').hide();
        $('#customer_div').hide();
        $('#mechanic_div').hide();
        $('#booking_div').show();
		$.ajax({
            url: "/service/bookings/",
            type: "GET",
            success: function(data){
            	console.log(data)
            	$("#booking_data").empty();
            	$.each(data, function(key, value) {
            		var paid = 'No'
            		if (value.whether_payment_recieved){
            			paid = 'Yes'
            		}
            		var tbl_data = '<tr>';
            		tbl_data += '<td>' + value.car + '</td>';
            		tbl_data += '<td>' + value.mechanic + '</td>';
            		tbl_data += '<td>' + value.customer + '</td>';
            		tbl_data += '<td>' + value.booking_id + '</td>';
            		tbl_data += '<td>' + value.date_of_service + '</td>';
            		tbl_data += '<td>' + paid + '</td>';
            		tbl_data += '<td>' + value.other_booking_details + '</td>';
                    tbl_data += '<td><button data-booking_id="'+ value.booking_id +'" id="delete_booking">Delete</button></td>';
                    tbl_data += '</tr>';
            		$(tbl_data).appendTo("#booking_data");
            	});
            }
        })
	}
})